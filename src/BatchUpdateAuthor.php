<?php

namespace Drupal\filter_by_type;

use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * ChangeAuthorAction.
 */
class BatchUpdateAuthor {
  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a \Drupal\change_author_action\BatchUpdateAuthor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function updateAuthor($allowed_authors, $newAuthor, $newDate, $changeDate, $select, &$context) {
    foreach ($allowed_authors as $key => $author) {
    $message = 'Update Node...';
    $results = [];
    $node = Node::load($key);
      if (is_numeric($select)) {
        $node = Node::load($key);
        $node->status = $select;
        $node->save();
      }
      if ($newDate) {
        $m = $node->getOwnerID();
        $node->uid = $m;
        $node->created = strtotime($newDate);
        $node->save();
      }
      if ($changeDate) {
        $m = $node->getOwnerID();
        $node->uid = $m;
        $node->changed = strtotime($changeDate);
        $node->save();
      }
      if ($newAuthor) {
        $node->set('uid', $newAuthor);
        $node->save();
      }
      $context['message'] = $message;
      $context['results'][] = $results;
    }
  }

  /**
   * Finish batch.
   *
   * @param bool $success
   *   Indicates whether the batch process was successful.
   * @param array $results
   *   Results information passed from the processing callback.
   * @param array $operations
   *   A list of the operations that had not been completed by the batch API.
   */
  public static function updateAuthorFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
       count($results),
       'One content updated.', '@count contents updated.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }
}
