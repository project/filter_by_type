<?php

namespace Drupal\filter_by_type\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Render\Markup;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Edit form for content type filter.
 */
class TypeEditForm extends FormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Pager\PagerManagerInterface definition.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The current Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a \Drupal\change_author_action\Form\BulkUpdateFieldsForm.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   Pager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type_manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(MessengerInterface $messenger, PagerManagerInterface $pagerManager, Request $request, EntityTypeManagerInterface $entity_type_manager, Connection $connection = NULL) {
    $this->messenger = $messenger;
    $this->pagerManager = $pagerManager;
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('pager.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_by_type_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $base_path = $this->request->getBaseUrl();
    $type = $this->request->query->get('content_type');
    $user_role = $this->request->query->get('users');
    $created_date = $this->request->query->get('start_date');
    $created_end_date = $this->request->query->get('c_end_date');
    $updated_date = $this->request->query->get('end_date');
    $updated_end_date = $this->request->query->get('u_end_date');
    $status_node = $this->request->query->get('status');

    $form['details'] = [
      '#type' => 'fieldset',
      '#attributes' => [],
    ];

    // To display them in a drop down.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $type_options = [];
    $type_options = ['-SELECT-'];
    foreach ($node_types as $key => $node_type) {
      $type_options[$node_type->id()] = $node_type->label();
      $content_type[] = $key;
    }
    $form['details']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Type'),
      '#description' => $this->t("Filter by content type"),
      '#default_value' => !empty($type) ? $type : '',
      '#options' => $type_options,
    ];

    // To get users list in drop down.
    $userStorage = $this->entityTypeManager->getStorage('user');
    $query = $userStorage->getQuery();
    $uids = $query
      ->condition('status', '1')
      ->accessCheck(TRUE)
      ->execute();
    $users = $userStorage->loadMultiple($uids);
    $author_options = [];
    $author_options = ['-SELECT-'];
    foreach ($users as $user) {
      $name = $user->get('name')->value;
      $author_options[] = $name;
    }
    $form['details']['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Author'),
      '#description' => $this->t("Filter by user name"),
      '#default_value' => !empty($user_role) ? $user_role : '',
      '#options' => $author_options,
    ];

    $form['details']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Status'),
      '#description' => $this->t("Filter by node status"),
      '#empty_option' => '-SELECT-',
      '#options' => [$this->t('Unpublish'), $this->t('Publish')],
      '#default_value' => $status_node,
    ];

    $form['details']['created_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#description' => $this->t("Select node created date"),
      '#default_value' => !empty($created_date) ? $created_date : '',
      '#max' => date('Y-m-d'),
    ];
    $form['details']['created_end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#description' => $this->t("Select node created date"),
      '#default_value' => !empty($created_end_date) ? $created_end_date : '',
      '#max' => date('Y-m-d'),
    ];

    $form['details']['updated_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#description' => $this->t("Select node updated date"),
      '#default_value' => !empty($updated_date) ? $updated_date : '',
      '#max' => date('Y-m-d'),
    ];
    $form['details']['updated_end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End Date'),
      '#description' => $this->t("Select node updated date"),
      '#default_value' => !empty($updated_end_date) ? $updated_end_date : '',
      '#max' => date('Y-m-d'),
    ];

    // The submit button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
      '#submit' => [[$this, 'submitForm']],
      '#validate' => [
        [$this, 'validateDataFilterForm'],
      ],
    ];

    // The reset button.
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
    ];

    if (!empty($user_role)) {
      $user = $this->entityTypeManager->getStorage('user')->load($user_role);
      $user_name = $user->get('name')->value;
    }

    $query = $this->connection->select('node_field_data', 'n');
    $query->innerJoin('users_field_data', 'u', 'n.uid=u.uid');
    $query->fields('n', ['nid', 'title', 'type', 'created', 'changed', 'status']);
    $query->fields('u', ['uid', 'name']);
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(25);

    if (!empty($type)) {
      $query->condition('n.type', $type);
    }
    if (!empty($user_name)) {
      $query->condition('u.name', $user_name, '=');
    }
    if ($created_date) {
      $query->condition('n.created', strtotime($created_date), '>=');
    }
    if ($created_end_date) {
      $query->condition('n.created', strtotime($created_end_date . '23:59:59'), '<=');
    }
    if ($updated_date) {
      $query->condition('n.changed', strtotime($updated_date), '>=');
    }
    if ($updated_end_date) {
      $query->condition('n.changed', strtotime($updated_end_date . '23:59:59'), '<=');
    }
    if (is_numeric($status_node)) {
      $query->condition('n.status', $status_node, '=');
    }
    $query = $query->orderby('n.changed', 'DESC');
    $results = $pager->execute()->fetchAll();

    $options = [];
    foreach ($results as $data) {
      $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $data->nid])->toString();
      $edit_link = new FormattableMarkup('<a href=":link">:label</a>', [':link' => $edit_url, ':label' => $this->t('Edit')]);
      $view_url = Url::fromRoute('entity.node.canonical', ['node' => $data->nid]);
      $view_link = Link::fromTextAndUrl($this->t('View'), $view_url)->toString();
      $links = new FormattableMarkup('@edit_link | @view_link', ['@edit_link' => $edit_link, '@view_link' => $view_link]);
      $options[$data->nid] = [
        'node_title' => Markup::create('<a href="' . $base_path . '/node/' . $data->nid . '">' . $data->title . '</a>'),
        'type' => $data->type,
        'author' => Markup::create('<a href="' . $base_path . '/user/' . $data->uid . '">' . $data->name . '</a>'),
        'status' => $data->status == 1 ? 'Published' : 'Unpublished',
        'created' => date('d-m-Y', $data->created),
        'changed' => date('d-m-Y', $data->changed),
        'opration' => $links,
      ];
    }

    $header_table = [
      'node_title' => $this->t('Title'),
      'type' => $this->t('Content Type'),
      'author' => $this->t('Author'),
      'status' => $this->t('Status'),
      'created' => $this->t('Created Date'),
      'changed' => $this->t('Updated Date'),
      'opration' => $this->t('Operations'),
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header_table,
      '#options' => $options,
      '#empty' => $this->t('No Data found')
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Action:'),
      '#empty_option' => '-SELECT-',
      '#options' => [
        'update_authors' => $this->t('Update Authors'),
        'update_node_status' => $this->t('Update Node Status'),
        'update_created_date' => $this->t('Update Created Date'),
        'update_changed_date' => $this->t('Update Change Date'),
      ],
      '#attributes' => [
        'name' => 'select_field',
      ],
    ];

    $form['authors'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Author'),
      '#options' => $author_options,
      '#attributes' => [
        'id' => 'author-name',
      ],
      '#states' => [
        'visible' => [
          [':input[name="select_field"]' => ['value' => 'update_authors']],
        ],
      ],
    ];

    $form['update_status'] = [
      '#type' => 'select',
      '#empty_option' => '-SELECT-',
      '#options' => [$this->t('Unpublish'), $this->t('Publish')],
      '#attributes' => [
        'id' => 'update-node-status',
      ],
      '#states' => [
        'visible' => [
          [':input[name="select_field"]' => ['value' => 'update_node_status']],
        ],
      ],
    ];

    $form['create'] = [
      '#type' => 'date',
      '#size' => '60',
      '#attributes' => [
        'id' => 'create-date',
      ],
      '#states' => [
        'visible' => [
          [':input[name="select_field"]' => ['value' => 'update_created_date']],
        ],
      ],
    ];

    $form['change'] = [
      '#type' => 'date',
      '#size' => '60',
      '#attributes' => [
        'id' => 'change-date',
      ],
      '#states' => [
        'visible' => [
          [':input[name="select_field"]' => ['value' => 'update_changed_date']],
        ],
      ],
    ];

    $form['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected items'),
      "#weight" => 2,
      '#button_type' => 'primary',
      '#submit' => [[$this, 'submitFormTwo']],
      '#validate' => [
        [$this, 'validateDataUpdateForm'],
      ],
    ];

    $form['#attached']['library'][] = 'filter_by_type/global_style';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDataFilterForm(array &$form, FormStateInterface $form_state) {
    $start_date = $form_state->getValue('created_date');
    $c_end_date = $form_state->getValue('created_end_date');
    $end_date = $form_state->getValue('updated_date');
    $u_end_date = $form_state->getValue('updated_end_date');

    if ($start_date && $c_end_date && ($c_end_date) < ($start_date)) {
      $form_state->setErrorByName('created_end_date', $this->t('Ensure end date is after or equal to start date'));
    }
    if ($end_date && $u_end_date && ($u_end_date) < ($end_date)) {
      $form_state->setErrorByName('updated_end_date', $this->t('Ensure end date is after or equal to start date'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateDataUpdateForm(array &$form, FormStateInterface $form_state) {
    $allowed_authors = array_filter($form_state->getValue('table'));
    if (empty($allowed_authors)) {
      $this->messenger()->addError($this->t('No Content Selected'));
    }

    $authors = $form_state->getValue('authors');
    $create = $form_state->getValue('create');
    $change = $form_state->getValue('change');
    $update_status = $form_state->getValue('update_status');

    if (!empty($allowed_authors) && empty($authors ?: $create ?: $change ?: is_numeric($update_status))) {
      $form_state->setErrorByName('operation', $this->t('No Action selected.'));
    }
    foreach ($allowed_authors as $key => $date) {
      $node = Node::load($key);
      $modified_date = $node->getChangedTime();
      $formatted_date = \Drupal::service('date.formatter')->format($modified_date, 'custom', 'Y-m-d');
      $newDate = $form_state->getValue('create');
      if ($newDate > $formatted_date) {
        $form_state->setErrorByName('create', $this->t('The created date cannot be greater than the modified date.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_type = $form_state->getValue('type');
    $users = $form_state->getValue('roles');
    $start_date = Xss::filter($form_state->getValue('created_date'));
    $c_end_date = Xss::filter($form_state->getValue('created_end_date'));
    if (!empty($start_date) && empty($c_end_date)) {
      $c_end_date = date('Y-m-d');
    }
    $end_date = Xss::filter($form_state->getValue('updated_date'));
    $u_end_date = Xss::filter($form_state->getValue('updated_end_date'));
    if (!empty($end_date) && empty($u_end_date)) {
      $u_end_date = date('Y-m-d');
    }
    $node_status = Xss::filter($form_state->getValue('status'));
    $url = Url::fromRoute('filter_by_type.display_table')->setRouteParameters(['content_type' => $content_type, 'users' => $users, 'start_date' => $start_date, 'c_end_date' => $c_end_date, 'end_date' => $end_date, 'u_end_date' => $u_end_date, 'status' => $node_status]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormTwo(array &$form, FormStateInterface $form_state) {
    $allowed_authors = array_filter($form_state->getValue('table'));
    $newAuthor = $form_state->getValue('authors');
    $newDate = $form_state->getValue('create');
    $changeDate = $form_state->getValue('change');
    $select = $form_state->getValue('update_status');

    $batch = [
      'title' => $this->t('Updating Node...'),
      'operations' => [
        [
          '\Drupal\filter_by_type\BatchUpdateAuthor::updateAuthor',
          [$allowed_authors, $newAuthor, $newDate, $changeDate, $select],
        ],
      ],
      'progress_message' => $this->t('Processed @current out of @total. Estimated time: @estimate.'),
      'finished' => '\Drupal\filter_by_type\BatchUpdateAuthor::updateAuthorFinishedCallback',
    ];
    if(!empty($allowed_authors || $newAuthor || $newDate || $changeDate || $select)) {
      batch_set($batch);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm($form, &$form_state) {
    $url = Url::fromRoute('filter_by_type.display_table');
    $form_state->setRedirectUrl($url);
  }
}
