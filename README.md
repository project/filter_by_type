## INTRODUCTION

The Filter By Type module is a Filter and Update module.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- The FIlter By Type module is a Filter module. Can Update Author name, Node status (Published/Unpublished) Node created date and Node modified date in multiple nodes. Apart from these if have a lots of nodes so we cane filter nodes selected by Content type, Authors, Node status (Published/Unpublished), Node created Date and Node modified date and update securely.	
- Configuration step #2
- Configuration step #3

## MAINTAINERS

Current maintainers for Drupal 10:

- Harshita Mehna (Harshita mehna) - https://www.drupal.org/u/harshita-mehna
